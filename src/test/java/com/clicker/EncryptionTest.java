package com.clicker;

import static com.clicker.utils.Common.writeObject;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.File;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.clicker.controller.EncryptionProvider;

public class EncryptionTest {
    private static final String TEST_STRING = "Hello World! This is encryption test, it can contains every data you want";
    private static final EncryptionProvider encryptionProvider = new EncryptionProvider();
    private static final Application app = new Application();
    private static final String password = "password";

    @ParameterizedTest()
    @MethodSource(value = "getTestData")
    void testEncryptionPassword(String data) {
        var firstEncryptionTry = encryptionProvider.encrypt(data.getBytes(), password.toCharArray());
        var secondEncryptionTry = encryptionProvider.encrypt(data.getBytes(), password.toCharArray());

        var firstDecrypted = encryptionProvider.decrypt(firstEncryptionTry, password.toCharArray());
        var secondDecrypted = encryptionProvider.decrypt(secondEncryptionTry, password.toCharArray());

        assertNotEquals(firstEncryptionTry, secondEncryptionTry);
        assertEquals(data, new String(firstDecrypted));
        assertArrayEquals(firstDecrypted, secondDecrypted);
    }

    @ParameterizedTest()
    @MethodSource(value = "getTestData")
    void testEncryptionPasswordFromFile(String data) {
        var encrypted = encryptionProvider.encrypt(data.getBytes(), password.toCharArray());
        writeObject("testSerialize", encrypted);

        var read = app.argsParser("-p password -f testSerialize".split(" "));

        assertEquals(data, read);
    }

    @ParameterizedTest()
    @MethodSource(value = "getTestData")
    void testEncryptionPasswordFromFileMultipleLines(String data) {
        var encrypted = encryptionProvider.encrypt(getSeveralLineString(data).getBytes(), password.toCharArray());
        writeObject("testSerializeSeveral", encrypted);

        var read = app.argsParser("-p password -f testSerializeSeveral".split(" "));

        assertEquals(data + data, read);
    }

    @ParameterizedTest()
    @MethodSource(value = "getTestData")
    void testEncryptionRSA(String data) {
        var keyPair = encryptionProvider.generateKeyRSA();

        var encrypted = encryptionProvider.encrypt(data.getBytes(), keyPair);
        var decrypted = encryptionProvider.decrypt(encrypted, keyPair);

        assertEquals(data, new String(decrypted));
    }

    @ParameterizedTest()
    @MethodSource(value = "getTestData")
    void testEncryptionRSAFromFile(String data) {
        var keyPair = encryptionProvider.generateKeyRSA();
        var encrypted = encryptionProvider.encrypt(data.getBytes(), keyPair);

        writeObject("keyRsa", keyPair);
        writeObject("encryptedFile", encrypted);

        String decrypted = app.argsParser("-f encryptedFile -k keyRsa".split(" "));
        assertEquals(data, decrypted);
    }

    @ParameterizedTest()
    @MethodSource(value = "getTestData")
    void testEncryptionRSAFromFileMultipleLines(String data) {
        var keyPair = encryptionProvider.generateKeyRSA();
        var encrypted = encryptionProvider.encrypt(getSeveralLineString(data).getBytes(), keyPair);

        writeObject("keyRsa2", keyPair);
        writeObject("encryptedFile2", encrypted);

        String decrypted = app.argsParser("-f encryptedFile2 -k keyRsa2".split(" "));
        assertEquals(data + data, decrypted);
    }

    @AfterEach
    void cleanUp() {
        new File("testSerialize").delete();
        new File("testSerializeSeveral").delete();
        new File("keyRsa").delete();
        new File("encryptedFile").delete();
        new File("keyRsa2").delete();
        new File("encryptedFile2").delete();
    }

    public static Stream<Arguments> getTestData() {
        return Stream.of(Arguments.of(TEST_STRING), Arguments.of(TEST_STRING.repeat(1000)));
    }

    private String getSeveralLineString(String data) {
        return (data + System.lineSeparator() + data);
    }
}
