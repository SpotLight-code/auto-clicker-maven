package com.clicker;

import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.clicker.utils.Parser;
import com.clicker.utils.PreProcessor;

class DemoApplicationTests {

    @Test
    void testEscape() {
        List<String> input = List.of("!'{fwe}'!{}!'{}'!",
                "123",
                "!'gergerger",
                "fwefwe'!",
                "{wait: 100}",
                "!'resdgreger'!",
                "!'{}{}{}{}{{{{}}}}}{{}{}{}{}{}{'!"
        );
        List<String> expected = List.of("\\{fwe\\}{}\\{\\}",
                "123",
                "!'gergerger",
                "fwefwe'!",
                "{wait: 100}",
                "resdgreger",
                "\\{\\}\\{\\}\\{\\}\\{\\}\\{\\{\\{\\{\\}\\}\\}\\}\\}\\{\\{\\}\\{\\}\\{\\}\\{\\}\\{\\}\\{"
        );

        IntStream.range(0, input.size())
                .forEach(i -> Assertions.assertEquals(expected.get(i), Parser.escapeBrackets(input.get(i))));
    }

    @Test
    void testParseLoop() {
        List<String> input = List.of(
                "{loop:3}te{endLoop}",
                "{loop:4}{tab}{endLoop}"
        );
        List<List<String>> expected = List.of(
                List.of("alt+tab", "wait:500", "t", "e", "t", "e", "t", "e"),
                List.of("alt+tab", "wait:500", "tab", "tab", "tab", "tab"));


        IntStream.range(0, input.size())
                .forEach(i -> Assertions.assertEquals(expected.get(i),
                        PreProcessor.parseLoop(new Parser().parse(input.get(i)))));

    }

}
