package com.clicker.utils;

import static java.util.stream.Collectors.joining;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import lombok.Getter;

public class ReplacerUtils {
    private static final String en = "qwertyuiop[]asdfghjkl;'zxcvbnm,.s";
    private static final String ru = "йцукенгшщзхъфывапролджэячсмитьбюі";
    @Getter
    private static final Map<Character, Character> upperCaseSymbols = new HashMap<>();

    static {
        upperCaseSymbols.put('!', '1');
        upperCaseSymbols.put('@', '2');
        upperCaseSymbols.put('#', '3');
        upperCaseSymbols.put('$', '4');
        upperCaseSymbols.put('%', '5');
        upperCaseSymbols.put('^', '6');
        upperCaseSymbols.put('&', '7');
        upperCaseSymbols.put('*', '8');
        upperCaseSymbols.put('(', '9');
        upperCaseSymbols.put(')', '0');
        upperCaseSymbols.put('_', '-');
        upperCaseSymbols.put('+', '=');
        upperCaseSymbols.put('{', '[');
        upperCaseSymbols.put('}', ']');
        upperCaseSymbols.put('"', '\'');
        upperCaseSymbols.put(':', ';');
        upperCaseSymbols.put('?', '/');
        upperCaseSymbols.put('>', '.');
        upperCaseSymbols.put('<', ',');
        upperCaseSymbols.put('~', '`');
    }

    public static String translate(String str) {
        return IntStream.range(0, str.length())
                .mapToObj(str::charAt)
                .map(String::valueOf)
                .map(ReplacerUtils::replaceCharacter)
                .collect(joining());
    }

    private static String replaceCharacter(String character) {
        if (!ru.contains(character) && !ru.contains(character.toLowerCase())) {
            return character;
        }
        return ru.contains(character) ?
                String.valueOf(en.charAt(ru.indexOf(character))) :
                String.format("{shift+%s}", en.charAt(ru.indexOf(character.toLowerCase())));
    }

    public static String transformUpperToLower(String s) {

        if (s.length() > 2) {
            StringBuilder sb = new StringBuilder(s);
            sb.setCharAt(6, upperCaseSymbols.getOrDefault(s.charAt(6), s.charAt(6)));
            s = sb.toString();
        }

        return s;
    }
}
