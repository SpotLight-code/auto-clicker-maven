package com.clicker.utils;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Parser {

    private static final Pattern lowerCaseSymbols = Pattern.compile("[a-z0-9./'\\]\\[\\\\;\\-=]");
    private static final Pattern protectedBracketOpen = Pattern.compile("\\{(?<!\\\\\\{)"); //
    private static final Pattern protectedBracketClose = Pattern.compile("}(?<!\\\\})");
    private static final String openSequence = "!'";
    private static final String closeSequence = "'!";
    private static final String ALT_TAB = "{alt+tab}{wait:500}";

    private int counter = 0;

    public List<String> parse(String input) {
        counter = 0;

        String inputWithAltTab = addAltTabWithDelayBeforeStart(escapeBrackets(input));

        //enhancement for empty string for test if no text between }{
        String replace = inputWithAltTab.replace("}{", "}{{");

        return Arrays.stream(protectedBracketOpen.split(replace))
                .flatMap(str -> Arrays.stream(protectedBracketClose.split(str)))
                .map(Parser::removeSpecialSymbols)
                .flatMap(this::proceedParser)
                .collect(toList());
    }

    private String addAltTabWithDelayBeforeStart(String input) {
        return ALT_TAB + input;
    }

    private static String removeSpecialSymbols(String str) {
        return str.replace("\\{", "{").replace("\\}", "}");
    }

    private Stream<String> proceedParser(String str) {
        return counter++ % 2 == 0 ? enhanceString(str).stream() : Stream.of(str);
    }

    private static List<String> enhanceString(String str) {
        String translatedString = ReplacerUtils.translate(str);
        if (!translatedString.equals(str)) {
            return new Parser().parse(translatedString);
        }
        return IntStream.range(0, str.length())
                .mapToObj(str::charAt)
                .map(String::valueOf)
                .map(c -> !lowerCaseSymbols.matcher(c).matches() ? "shift+" + c : c)
                .map(ReplacerUtils::transformUpperToLower)
                .collect(toList());
    }

    public static String escapeBrackets(String input) {
        return buildStringRecursive(input);
    }

    private static String buildStringRecursive(String str) {
        int startIndex = str.indexOf(openSequence);
        if (startIndex != -1) {
            int endIndex = str.indexOf(closeSequence, startIndex);
            if (endIndex != -1) {
                return buildStringRecursive(
                        str.substring(0, startIndex) +
                                enhanceSpecialSymbols(str.substring(startIndex + 2, endIndex)) +
                                str.substring(endIndex + 2));
            }
        }
        return str;
    }

    private static String enhanceSpecialSymbols(String substring) {
        return substring.replace("{", "\\{").replace("}", "\\}");
    }
}