package com.clicker.utils;


import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.clicker.entity.ClickEvent;

import lombok.SneakyThrows;

public class Clicker {

    @SneakyThrows
    public static void clicker(List<String> buttons) {
        var robot = new Robot();
        buttons.stream()
                .map(str -> Arrays.asList(str.split("\\+")))
                .map(List::stream)
                .forEach(stream -> {
                    List<ClickEvent> buttonsToPress = stream.map(ClickEvent::new)
                            .collect(Collectors.toList());
                    try {
                        buttonsToPress.forEach(c -> c.press(robot));
                        waitMs(10);
                        buttonsToPress.forEach(c -> c.release(robot));
                        waitMs(10);
                    } catch (Exception e) {
                        buttonsToPress.forEach(c -> c.release(robot));
                    }
                });
    }

    @SneakyThrows
    public static void waitMs(long time) {
        Thread.sleep(time);
    }

}
