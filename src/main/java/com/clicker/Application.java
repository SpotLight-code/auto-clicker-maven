package com.clicker;

import static com.clicker.utils.Common.readObject;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.clicker.controller.EncryptionProvider;
import com.clicker.utils.Parser;
import com.clicker.utils.PreProcessor;

import lombok.SneakyThrows;

public class Application {

    private final Parser parser = new Parser();

    private final EncryptionProvider encryptionProvider = new EncryptionProvider();

    @SneakyThrows
    public static void main(String[] args) {
        new Application().launch(args);
    }

    @SneakyThrows
    private void launch(String[] args) {
        List<String> parseToList = parser.parse(argsParser(args));
        PreProcessor.begin(parseToList);
    }

    @SneakyThrows
    public String argsParser(String[] args) {
        if (!args[0].equals("-f") && !args[0].equals("-k") && !args[0].equals("-p")) {
            return String.join(" ", args);
        }
        byte[] result = null;
        Object decryptionKey = null;
        boolean password = true;
        Map<String, String> replacers = new HashMap<>();

        for (int i = 0; i < args.length; i++) {
            if (Objects.equals(args[i], "-f")) {
                result = Files.readAllBytes(Paths.get(args[++i]));
            } else if (args[i].equals("-k")) {
                decryptionKey = readObject(args[++i]);
                password = false;
            } else if (args[i].equals("-p")) {
                decryptionKey = args[++i];
            } else if (args[i].contains(":")) {
                var keyVal = args[i].split(":");
                replacers.put(keyVal[0], keyVal[1]);
            }
        }

        String script = encryptionProvider.decrypt(decryptionKey, result, password);

        for (var replacer : replacers.entrySet()) {
            script = script.replace(replacer.getKey(), replacer.getValue());
        }

        return script;
    }
}
