package com.clicker.entity;

import static java.lang.Integer.parseInt;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

public class ClickEvent {
    private static final Map<String, Integer> map = new HashMap<>();

    static {
        map.put("tab", KeyEvent.VK_TAB);
        map.put("shift", KeyEvent.VK_SHIFT);
        map.put("space", KeyEvent.VK_SPACE);
        map.put("%", KeyEvent.VK_5);
        map.put("enter", KeyEvent.VK_ENTER);
        map.put("down", KeyEvent.VK_DOWN);
        map.put("ctrl", KeyEvent.VK_CONTROL);
        map.put("alt", KeyEvent.VK_ALT);
        map.put("left", MouseEvent.BUTTON1_DOWN_MASK);
        map.put("right", MouseEvent.BUTTON3_DOWN_MASK);
        map.put("leftBtn", KeyEvent.VK_LEFT);
        map.put("rightBtn", KeyEvent.VK_RIGHT);
        map.put("upBtn", KeyEvent.VK_UP);
    }

    private final EventType eventType;
    private Integer scanCode;

    private Integer x;
    private Integer y;

    public ClickEvent(String clickEvent) {
        if (!clickEvent.contains(":")) {
            this.eventType = EventType.buttonClick;
            this.scanCode = map.getOrDefault(clickEvent,
                    KeyEvent.getExtendedKeyCodeForChar(clickEvent.charAt(0)));
        } else {
            var typeWithData = clickEvent.split(":");
            eventType = EventType.valueOf(typeWithData[0].trim());

            if (eventType.equals(EventType.mouseClick)) {
                this.scanCode = map.get(typeWithData[1].trim());
            } else {
                var coordinates = typeWithData[1].split(",");
                this.x = parseInt(coordinates[0].trim());
                this.y = parseInt(coordinates[1].trim());
            }
        }
    }

    public void press(Robot robot) {
        switch (eventType) {
            case mouseSeek:
                robot.mouseMove(x, y);
                break;
            case buttonClick:
                robot.keyPress(scanCode);
                break;
            case mouseClick:
                robot.mousePress(scanCode);
        }
    }

    public void release(Robot robot) {
        switch (eventType) {
            case buttonClick:
                robot.keyRelease(scanCode);
                break;
            case mouseClick:
                robot.mouseRelease(scanCode);
        }
    }
}
