package com.clicker.entity.encryption;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EncryptedData implements Serializable {
    private static final long serialVersionUID = 4567890324L;

    private byte[] data;
    private byte[] additionalData;
}
